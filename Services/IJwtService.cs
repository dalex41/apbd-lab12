using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
namespace lab12.Services;

public interface IJwtService {
    public JwtSecurityToken GetNewJwtToken(Claim[] claims, DateTime expDate);
}
