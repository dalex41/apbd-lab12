using lab12.Models;
using lab12.Models.DTOs;
using lab12.Securities;
using lab12.Repositories;
namespace lab12.Services;

public class UserService : IUserService {
    private readonly IUserRepository _ur;
    public UserService(IUserRepository ur) {
        _ur = ur;
    }
    
    public async Task AddUser(User newUser) {
        await _ur.AddUser(newUser);
    }

    private bool VerifyPassword(User user, string enteredPw) {
        return UserSecurities.GetHashedPasswordWithSalt(enteredPw, user.Salt)
            .Equals(user.PasswordHash);
    }

    public async Task<bool> IsAuthorized(string login, string pw) {
        User user = await _ur.GetUserFromLogin(login);
        return VerifyPassword(user, pw);
    }

    public async Task SetRefTokenDays(RegisterUserDTO registerUser, string refToken, int expDays) {
        await _ur.SetRefTokenDays(await _ur.GetUserFromLogin(registerUser.Login),
                refToken, expDays);
    }
    public async Task SetRefTokenDays(RefreshTokenDTO refreshToken, string refToken, int expDays) {
        await _ur.SetRefTokenDays(await _ur.GetUserFromRefToken(refreshToken.RefreshToken),
                refToken, expDays);
    }
}
