using lab12.Models;
using lab12.Models.DTOs;
namespace lab12.Services;

public interface IUserService {
    public Task AddUser(User newUser);
    public Task<bool> IsAuthorized(string login, string pw);
    public Task SetRefTokenDays(RegisterUserDTO registerUser, string refToken, int expDays);
    public Task SetRefTokenDays(RefreshTokenDTO refreshToken, string refToken, int expDays);
}
