using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
namespace lab12.Services;

public class JwtService : IJwtService {
    private readonly IConfiguration _iconf;
    public JwtService(IConfiguration iconf) {_iconf = iconf; }

    public JwtSecurityToken GetNewJwtToken(Claim[] claims, DateTime expDate) {
        SymmetricSecurityKey key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_iconf["JWT:Key"]!));
        SigningCredentials creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

        JwtSecurityToken token = new JwtSecurityToken(
            issuer: "https://localhost:5001",
            audience: "https://localhost:5001",
            claims: claims,
            expires: expDate,
            signingCredentials: creds
        );
        return token;
    }
}
