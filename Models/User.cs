using System.ComponentModel.DataAnnotations;
namespace lab12.Models;

public class User {
    [Key]
    public int ID {get; set; }
    public string Login { get; set; } = null!;
    public string PasswordHash { get; set; } = null!;
    public string Salt { get; set; } = null!;
    public string RefreshToken { get; set; } = null!;
    public DateTime? RefreshTokenExpiry { get; set; } = null!;
}
