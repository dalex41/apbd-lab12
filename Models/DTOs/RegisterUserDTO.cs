using System.ComponentModel.DataAnnotations;
namespace lab12.Models.DTOs;

public class RegisterUserDTO {
    [Required]
    public string Login {get; set; } = null!;
    [Required]
    public string Password {get; set; } = null!;
}
