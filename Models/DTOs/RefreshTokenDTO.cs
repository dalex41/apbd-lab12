using System.ComponentModel.DataAnnotations;
namespace lab12.Models.DTOs;

public class RefreshTokenDTO {
    [Required]
    public string JwtAccessToken { get; set; } = null!;
    [Required]
    public string RefreshToken { get; set; } = null!;
}
