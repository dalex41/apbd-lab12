namespace lab12.Exceptions;

public class GeneralException {
    public string Message { get; set; } = null!;
    public string Details { get; set; } = null!;

}
