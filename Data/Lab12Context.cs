using Microsoft.EntityFrameworkCore;
using lab12.Models;
namespace lab12.Data;

public partial class Lab12Context : DbContext
{
    public Lab12Context() {}

    public Lab12Context(DbContextOptions<Lab12Context> options) : base(options) {}

    public DbSet<User> Users { get; set; }
}
