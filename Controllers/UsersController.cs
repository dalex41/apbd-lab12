using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using lab12.Models.DTOs;
using lab12.Models;
using System.IdentityModel.Tokens.Jwt;
using lab12.Securities;
using lab12.Services;
namespace lab12.Controllers;

[ApiController]
[Route("api/[controller]")]
public class UsersController : ControllerBase {
    private readonly IUserService _us;
    private readonly IJwtService _jwt;
    private readonly IConfiguration _iconf;
    public UsersController(IUserService us, IJwtService jwt, IConfiguration iconf) {
        _us = us;
        _jwt = jwt;
        _iconf = iconf;
    }

    [AllowAnonymous]
    [HttpPost("register")]
    public async Task<IActionResult> RegisterUser(RegisterUserDTO user) {
        Tuple<string, string> hashPwSalt = UserSecurities.GetHashAndSalt(user.Password);
        User newUser = new User() {
            Login = user.Login,
            PasswordHash = hashPwSalt.Item1,
            Salt = hashPwSalt.Item2,
            RefreshToken = UserSecurities.GenerateRefreshToken(),
            RefreshTokenExpiry = DateTime.Now.AddHours(1)
        };
        await _us.AddUser(newUser);
        return Ok();
    }

    [AllowAnonymous]
    [HttpPost("login")]
    public async Task<IActionResult> Login(RegisterUserDTO userLogin)
    {

        if (!await _us.IsAuthorized(userLogin.Login, userLogin.Password))
        { return Unauthorized("Wrong login or password"); }

        Claim[] claims = new[]
        {
            new Claim(ClaimTypes.Role, "user"),
        };

        JwtSecurityToken token = _jwt.GetNewJwtToken(claims, DateTime.Now.AddMinutes(30));
    
        string refreshToken = UserSecurities.GenerateRefreshToken();
        await _us.SetRefTokenDays(userLogin, refreshToken, 1);

        return Ok(new
        {
            accessToken = new JwtSecurityTokenHandler().WriteToken(token),
            refreshToken = refreshToken
        });
    }

    [HttpPost("refresh")]
    public async Task<IActionResult> Refresh(RefreshTokenDTO refreshTokenDTO) {

        Claim[] claims = new[]
        {
            new Claim(ClaimTypes.Role, "user"),
        };

        JwtSecurityToken token = _jwt.GetNewJwtToken(claims, DateTime.Now.AddMinutes(30));
    
        string refreshToken = UserSecurities.GenerateRefreshToken();
        await _us.SetRefTokenDays(refreshTokenDTO, refreshToken, 1);

        return Ok(new
        {
            accessToken = new JwtSecurityTokenHandler().WriteToken(token),
            refreshToken = refreshToken
        });
    }
}
