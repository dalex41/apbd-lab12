using lab12.Models;
using lab12.Data;
using Microsoft.IdentityModel.Tokens;
using Microsoft.EntityFrameworkCore;
namespace lab12.Repositories;

public class UserRepository : IUserRepository {
    private readonly Lab12Context _ctx;
    public UserRepository(Lab12Context ctx) {
        _ctx = ctx;
    }

    public async Task AddUser(User newUser) {
        await _ctx.Users.AddAsync(newUser);
        await _ctx.SaveChangesAsync();
    }

    public async Task<User> GetUserFromLogin(string login) {
        User? user = 
        await _ctx.Users.Where(u => u.Login.Equals(login))
            .SingleOrDefaultAsync();
        if (user == null)
            throw new ArgumentException("User "+login+" not found");
        return user;
    }

    public async Task<User> GetUserFromRefToken(string refToken) {
        User? user = await _ctx.Users.Where(u => u.RefreshToken.Equals(refToken))
            .SingleOrDefaultAsync();
        if (user == null)
            throw new SecurityTokenException("No refreshToken "+refToken+" found");
        return user;
    }

    public async Task<User> GetUserByValidRefToken(string refToken) {
        User user = await GetUserFromRefToken(refToken);
         if (user.RefreshTokenExpiry < DateTime.Now)
            throw new SecurityTokenException("Refresh token has expired");
         return user;
    }

    public async Task SetRefTokenDays(User user, string refToken, int expDays) {
        user.RefreshToken = refToken;
        user.RefreshTokenExpiry = DateTime.Now.AddDays(expDays);
        await _ctx.SaveChangesAsync();
    }
}
