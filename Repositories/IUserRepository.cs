using lab12.Models;
namespace lab12.Repositories;

public interface IUserRepository {
    public Task AddUser(User newUser);
    public Task<User> GetUserFromLogin(string login);
    public Task<User> GetUserFromRefToken(string refToken);
    public Task SetRefTokenDays(User user, string refToken, int expDays);
    public Task<User> GetUserByValidRefToken(string refToken);
}
